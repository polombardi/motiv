import { t, CommonActions, LoginPage} from '../common/commonExport';

export default class Page {
    constructor() {
        this.commonAction = new CommonActions();
        this.loginPage = new LoginPage();
        
    }

    async goto(url) {
        await t
            .navigateTo(url)
            .wait(3000);
    }

    async maximize() {
        await t
            .maximizeWindow();
        await this.refreshPage();
    }

    async refreshPage(){
        await t.eval(() => location.reload(true));
    }

    async sendUpdate(){
        await t.Click ()
    }



}