import { Selector, CommonActions } from '../common/commonExport';

export default class LoginPage {
    constructor() {
        this.commonActions = new CommonActions();
        this.inputUsername = Selector('[name="username"]');
        this.inputPassword = Selector('[name="password"]');
        this.signInBttn = Selector('[class="btn btn-green center-block"]');
        this.mood = Selector('[ng-click="ctrl.moodClicked(6)"]');
        this.confirmMood = Selector('[class="glyphicon glyphicon-ok"]');
        
        

        
    }
    async loginApp(user){
        await this.commonActions.typeTextCommon(user.username, this.inputUsername)
        await this.commonActions.typeTextCommon(user.password,this.inputPassword)
        await this.commonActions.commonClick(this.signInBttn);
    }
}