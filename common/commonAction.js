import {t, replace} from './commonExport'

export default class CommonActions{
    constructor() {
    }

    async commonClick(element) {   
        await t.click(element);
    }


    async typeTextCommon(text,element){
        await t.typeText(element, text, replace);
    }

} 