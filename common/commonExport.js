

export { default as Page } from '../page-models/page';
export { default as LoginPage } from '../page-models/loginPage';

//Common Elements and common pages
export { Selector, t, ClientFunction } from 'testcafe';
export { parseArguments } from '../utils/parseArguments';
export { default as CommonActions } from '../common/commonAction';


export {default as RequestHandler } from '../utils/requestHandler'

//Files
export { default } from '../config';
export { default as users } from '../users';

//Utils
//need to be exported,the following: pagination,FilterCategoryDiscover, 
export { uniqueId, paste, replace, timeout, longTimeout, getLabels ,capitalize} from '../utils/helperFunctions';