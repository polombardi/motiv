
const url = require('url');

export default class RequestHandler{
    constructor() {
        
    }
    
    async request(address, options = { method: 'get'},body){
        return new Promise((resolve, reject) => {
            options = this.addURLtoOptions(address,options);
            const lib = address.startsWith('https') ? require('https') : require('http');
            const request = lib.request(address, (response) => {
                response.setEncoding('utf8');
                const responseBody = [];
                response.on('data', (chunk) => responseBody.push(chunk));
                response.on('end', () => {
                    let joinedData;
                    try
                    {
                        joinedData = responseBody.join('');
                        resolve(JSON.parse(joinedData));
                    }
                    catch(err)
                    {
                        reject(`Could not parse the Json data:
                        Response: ${joinedData}
                        Error: ${err}`);
                    }
                });
                
            });
            request.on('error', (err) => reject(err));
            request.end();
        });
    }

    addURLtoOptions(address,options)
    {   
        const myURL = url.parse(address);
        options['hostname'] = myURL.host;
        options['path'] = myURL.path;
        return options;
    }

    //This request must be used to make post request using Content-Type JSON
    async requestJsonBody(url,options,body){      
        return new Promise((resolve, reject)=>{
            const https = url.startsWith('https') ? require('http') : require('http');
            let postData = body;
            options = this.addURLtoOptionsJson(url, options)
            let req = https.request(options, (res) => {
                res.setEncoding('utf8');
                const responseBody = [];
                res.on('data', (d) => {responseBody.push(d)});
                res.on('end', ()=>{
                    let data = responseBody.join('');
                    resolve(JSON.parse(data));
                })
            });
            req.on('error', (err) => reject(err));
            req.write(postData);
            req.end();
        });                                                                                                                                                                                      
    }

    addURLtoOptionsJson(address,options)
    {   
        const myURL = url.parse(address);
        options['hostname'] = myURL.hostname;
        options['path'] = myURL.path;
        options['protocol'] = myURL.protocol;
        options['port'] = myURL.port;
        return options;
    }
}